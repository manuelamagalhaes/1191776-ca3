FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://manuelamagalhaes@bitbucket.org/manuelamagalhaes/1191776-ca3.git

WORKDIR /tmp/build/1191776-ca3

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/ca2part2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
