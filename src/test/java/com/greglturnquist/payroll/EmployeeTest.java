package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getJobTitleEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        String result = employee.getJobTitle();

        //Assert
        assertEquals("Junior", result);
    }

    @Test
    void getJobTitleNotEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        String result = employee.getJobTitle();

        //Assert
        assertNotEquals("Senior", result);
    }

    @Test
    void setJobTitleEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        employee.setJobTitle("Senior");

        //Assert
        String result = employee.getJobTitle();
        assertEquals("Senior", result);
    }

    @Test
    void setJobTitleNotEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        employee.setJobTitle("Senior");

        //Assert
        String result = employee.getJobTitle();
        assertNotEquals("Junior", result);
    }

    @Test
    void getJobTitleNull() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee(null, "Baggins", "ring bearer", null, "1191776@isep.ipp.pt"));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getJobTitleEmpty() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("", "Baggins", "ring bearer", "", "1191776@isep.ipp.pt"));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getFirstNameNull() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee(null, "Baggins", "ring bearer", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getFirstNameEmpty() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("", "Baggins", "ring bearer", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getLastNameNull() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", null, "ring bearer", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getLastNameEmpty() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "", "ring bearer", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getDescriptionNull() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins", null, "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getDescriptionEmpty() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins", "", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void employeeEmailEmpty() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins", "ring bearer", "Junior", ""));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void employeeEmailNull() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins", "ring bearer", "Junior", null));

        //Assert
        assertEquals("Invalid field", exception.getMessage());
    }

    @Test
    void getEmailEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        String result = employee.getEmail();

        //Assert
        assertEquals("1191776@isep.ipp.pt", result);
    }

    @Test
    void getEmailNotEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        String result = employee.getEmail();

        //Assert
        assertNotEquals("msm@isep.ipp.pt", result);
    }

    @Test
    void setEmailEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "msm@isep.ipp.pt");

        //Act
        employee.setEmail("1191776@isep.ipp.pt");
        String result = employee.getEmail();

        //Assert
        assertEquals("1191776@isep.ipp.pt", result);
    }

    @Test
    void setEmailNotEquals() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        employee.setEmail("msm@isep.ipp.pt");
        String result = employee.getEmail();

        //Assert
        assertNotEquals("1191776@isep.ipp.pt", result);
    }

    @Test
    void setEmailNull() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> employee.setEmail(null));

        String email = employee.getEmail();

        //Assert
        assertAll(
                () -> assertEquals("Invalid field", exception.getMessage()),
                () -> assertEquals("1191776@isep.ipp.pt", email)
        );
    }

    @Test
    void setEmailEmpty() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> employee.setEmail(""));

        String email = employee.getEmail();

        //Assert
        assertAll(
                () -> assertEquals("Invalid field", exception.getMessage()),
                () -> assertEquals("1191776@isep.ipp.pt", email)
        );
    }

    @Test
    void employeeInvalidEmail() {
        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776.isep.ipp.pt"));

        //Assert
        assertEquals("Invalid email", exception.getMessage());
    }

    @Test
    void setEmailInvalid() {

        //Arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Junior", "1191776@isep.ipp.pt");

        //Act
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> employee.setEmail("1191776/isep.ipp.pt"));

        String email = employee.getEmail();

        //Assert
        assertAll(
                () -> assertEquals("Invalid email", exception.getMessage()),
                () -> assertEquals("1191776@isep.ipp.pt", email)
        );
    }
}
